
public class Noeud {
    private int[][] matrice;
    private boolean max;
    private int h;

    public Noeud(boolean max, int[][] matrice) {
        this.matrice = matrice;
        this.max = max;
    }

    public int getH() {
        return this.h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int[][] getMatrice() {
        return this.matrice;
    }

    public boolean isMax() {
        return this.max;
    }

    public String toString() {
        String r = " H : " + this.h + " Max : " + this.max + "\n Matrice : \n";
        for (int i = this.matrice.length - 1; i >= 0; i--) {
            for (int j = 0; j < this.matrice.length; j++) {
                r += this.matrice[i][j] + " ";
            }
            r += "\n";
        }
        return r;
    }

    public int quatrePionsAlignesLigne(boolean typeJoueur) {
        int retour = 0;
        boolean trouv = false;
        int jeton;
        if (typeJoueur)
            jeton = 1;
        else
            jeton = 2;
        int tmp = 0;
        for (int i = 0; i < this.matrice.length; i++) {
            for (int j = 0; j < this.matrice[i].length; j++) {
                if (matrice[i][j] == jeton) {
                    trouv = true;
                } else {
                    trouv = false;
                    tmp = 0;
                }
                if (matrice[i][j] == jeton & trouv)
                    tmp++;
                if (tmp == 4)
                    retour = 6666;
            }
            tmp = 0;
        }

        return retour;
    }

    public int quatrePionsAlignesColonne(boolean typeJoueur) {
        int retour = 0;
        boolean trouv = false;
        int jeton;
        if (typeJoueur)
            jeton = 1;
        else
            jeton = 2;
        int tmp = 0;
        for (int j = 0; j < this.matrice[j].length; j++) {
            for (int i = 0; i < this.matrice.length; i++) {
                if (matrice[i][j] == jeton) {
                    trouv = true;
                } else {
                    trouv = false;
                    tmp = 0;
                }
                if (matrice[i][j] == jeton & trouv)
                    tmp++;
                if (tmp == 4)
                    retour = 6666;
            }
            tmp = 0;
        }

        return retour;
    }

    public int quatrePionsAlignesDiagonal(boolean typeJoueur) {
        // pour le diagonal gauche:de bas à gauche vers haut à droite
        int jeton;
        if (typeJoueur)
            jeton = 1;
        else
            jeton = 2;
        int NB_COLONNES = this.matrice.length;
        int NB_LIGNES = this.matrice[0].length;
        for (int ligne = 0; ligne < NB_LIGNES - 3; ligne++) {
            for (int colonne = 0; colonne < NB_COLONNES - 3; colonne++) {
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne + 1] == jeton
                        && this.matrice[colonne + 2][ligne + 2] == jeton
                        && this.matrice[colonne + 3][ligne + 3] == jeton)) {
                    return 6666;
                }
            }
        }
        for (int ligne = 0; ligne < NB_LIGNES - 3; ligne++) {
            for (int colonne = NB_COLONNES - 1; colonne >= 3; colonne--) {
                if (this.matrice[colonne][ligne] == jeton && this.matrice[colonne - 1][ligne + 1] == jeton
                        && this.matrice[colonne - 2][ligne + 2] == jeton
                        && this.matrice[colonne - 3][ligne + 3] == jeton) {
                    return 6666;
                }
            }
        }
        return 0;
    }

    public int quatrePionsPossiblesColonne(boolean typeJoueur) {
        int resultat = 0;

        String jeton;
        if (typeJoueur)
            jeton = "1";
        else
            jeton = "2";

        for (int i = 0; i < this.matrice.length; i++) {
            String column = "";
            for (int j = 0; j < this.matrice[i].length; j++) {
                column += this.matrice[i][j];
            }
            if (column.contains(jeton + jeton + jeton + "0"))
                resultat += 1333;
            if (column.contains(jeton + jeton + "0"))
                resultat += 200;
            if (column.contains(jeton + "0"))
                resultat += 30;
        }

        return resultat;
    }

    public int quatrePionsPossiblesLigne(boolean typeJoueur) {
        int resultat = 0;
        int NB_COLONNES = this.matrice.length;
        int NB_LIGNES = this.matrice[0].length;
        int jeton;
        if (typeJoueur)
            jeton = 1;
        else
            jeton = 2;

        for (int ligne = 0; ligne < NB_LIGNES - 3; ligne++) {
            for (int colonne = 0; colonne < NB_COLONNES - 3; colonne++) {
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne] == jeton
                        && this.matrice[colonne + 2][ligne] == jeton && this.matrice[colonne + 3][ligne] == 0)) {
                    if (ligne == 0) {
                        resultat += 1333;
                    } else if (this.matrice[colonne + 3][ligne - 1] != 0) {
                        resultat += 1333;
                    }
                }
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne] == jeton
                        && this.matrice[colonne + 2][ligne] == 0 && this.matrice[colonne + 3][ligne] == jeton)) {
                    if (ligne == 0)
                        resultat += 1333;
                    else if (this.matrice[colonne + 2][ligne - 1] != 0) {
                        resultat += 1333;
                    }
                }
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne] == 0
                        && this.matrice[colonne + 2][ligne] == jeton && this.matrice[colonne + 3][ligne] == jeton)) {
                    if (ligne == 0)
                        resultat += 1333;
                    else if (this.matrice[colonne + 1][ligne - 1] != 0) {
                        resultat += 1333;
                    }
                }
                if ((this.matrice[colonne][ligne] == 0 && this.matrice[colonne + 1][ligne] == jeton
                        && this.matrice[colonne + 2][ligne] == jeton && this.matrice[colonne + 3][ligne] == jeton)) {
                    if (ligne == 0)
                        resultat += 1333;
                    else if (this.matrice[colonne][ligne - 1] != 0) {
                        resultat += 1333;
                    }
                }
            }
        }
        for (int ligne = 0; ligne < NB_LIGNES - 2; ligne++) {
            for (int colonne = 0; colonne < NB_COLONNES - 2; colonne++) {
                if ((this.matrice[colonne][ligne] == 0 && this.matrice[colonne + 1][ligne] == jeton
                        && this.matrice[colonne + 2][ligne] == jeton)) {
                    if (ligne == 0)
                        resultat += 200;
                    else if (this.matrice[colonne][ligne - 1] != 0) {
                        resultat += 200;
                    }
                }
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne] == 0
                        && this.matrice[colonne + 2][ligne] == jeton)) {
                    if (ligne == 0)
                        resultat += 200;
                    else if (this.matrice[colonne + 1][ligne - 1] != 0) {
                        resultat += 200;
                    }
                }
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne] == jeton
                        && this.matrice[colonne + 2][ligne] == 0)) {
                    if (ligne == 0)
                        resultat += 200;
                    else if (this.matrice[colonne + 2][ligne - 1] != 0) {
                        resultat += 200;
                    }
                }
            }
        }
        for (int ligne = 0; ligne < NB_LIGNES; ligne++) {
            for (int colonne = 0; colonne < NB_COLONNES - 1; colonne++) {
                if ((this.matrice[colonne][ligne] == 0 && this.matrice[colonne + 1][ligne] == jeton)) {
                    if (ligne == 0)
                        resultat += 30;
                    else if (this.matrice[colonne][ligne - 1] != 0)
                        resultat += 30;
                }
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne] == 0)) {
                    if (ligne == 0)
                        resultat += 30;
                    else if (this.matrice[colonne + 1][ligne - 1] != 0)
                        resultat += 30;
                }
            }
        }
        return resultat;
    }

    public int quatrePionsPossiblesDiagonal(boolean typeJoueur) {
        int resultat = 0;
        int NB_COLONNES = this.matrice.length;
        int NB_LIGNES = this.matrice[0].length;
        int jeton;
        if (typeJoueur)
            jeton = 1;
        else
            jeton = 2;
        // Un jeton
        for (int ligne = 0; ligne < NB_LIGNES - 1; ligne++) {
            for (int colonne = 0; colonne < NB_COLONNES - 1; colonne++) {
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne + 1] == 0)) {
                    if (this.matrice[colonne + 1][ligne] != 0)
                        resultat += 30;
                }
                if ((this.matrice[colonne][ligne] == 0 && this.matrice[colonne + 1][ligne + 1] == jeton)) {
                    if (ligne == 0)
                        resultat += 30;
                    else {
                        if (this.matrice[colonne][ligne - 1] != 0)
                            resultat += 30;
                    }
                }
            }
        }
        for (int ligne = 0; ligne < NB_LIGNES - 1; ligne++) {
            for (int colonne = NB_COLONNES - 1; colonne >= 1; colonne--) {
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne - 1][ligne + 1] == 0)) {
                    if (this.matrice[colonne - 1][ligne] != 0)
                        resultat += 30;
                }
                if ((this.matrice[colonne][ligne] == 0 && this.matrice[colonne - 1][ligne + 1] == jeton)) {
                    if (ligne == 0)
                        resultat += 30;
                    else {
                        if (this.matrice[colonne][ligne - 1] != 0)
                            resultat += 30;
                    }
                }
            }
        }
        // Deux jetons
        for (int ligne = 0; ligne < NB_LIGNES - 2; ligne++) {
            for (int colonne = 0; colonne < NB_COLONNES - 2; colonne++) {
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne + 1] == jeton
                        && this.matrice[colonne + 2][ligne + 2] == 0)) {
                    if (this.matrice[colonne + 2][ligne + 1] != 0) {
                        resultat += 200;
                    }
                }
                if ((this.matrice[colonne][ligne] == 0 && this.matrice[colonne + 1][ligne + 1] == jeton
                        && this.matrice[colonne + 2][ligne + 2] == jeton)) {
                    if (ligne == 0) {
                        resultat += 200;
                    } else {
                        if (this.matrice[colonne][ligne - 1] != 0)
                            resultat += 200;
                    }
                }
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne + 1] == 0
                        && this.matrice[colonne + 2][ligne + 2] == jeton)) {
                    if (this.matrice[colonne + 1][ligne] != 0) {
                        resultat += 200;
                    }
                }
            }
        }
        for (int ligne = 0; ligne < NB_LIGNES - 2; ligne++) {
            for (int colonne = NB_COLONNES - 1; colonne >= 2; colonne--) {
                if (this.matrice[colonne][ligne] == jeton && this.matrice[colonne - 1][ligne + 1] == jeton
                        && this.matrice[colonne - 2][ligne + 2] == 0) {
                    if (this.matrice[colonne - 2][ligne + 1] != 0)
                        resultat += 200;
                }
                if (this.matrice[colonne][ligne] == 0 && this.matrice[colonne - 1][ligne + 1] == jeton
                        && this.matrice[colonne - 2][ligne + 2] == jeton) {
                    if (ligne == 0)
                        resultat += 200;
                    else {
                        if (this.matrice[colonne][ligne - 1] != 0)
                            resultat += 200;
                    }
                }
                if (this.matrice[colonne][ligne] == jeton && this.matrice[colonne - 1][ligne + 1] == 0
                        && this.matrice[colonne - 2][ligne + 2] == jeton) {
                    if (this.matrice[colonne - 1][ligne] != 0)
                        resultat += 200;
                }
            }
        }
        // Trois jetons
        for (int ligne = 0; ligne < NB_LIGNES - 3; ligne++) {
            for (int colonne = 0; colonne < NB_COLONNES - 3; colonne++) {
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne + 1] == jeton
                        && this.matrice[colonne + 2][ligne + 2] == jeton
                        && this.matrice[colonne + 3][ligne + 3] == 0)) {
                    if (this.matrice[colonne + 3][ligne + 2] != 0) {
                        resultat += 1333;
                    }
                }
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne + 1] == jeton
                        && this.matrice[colonne + 2][ligne + 2] == 0
                        && this.matrice[colonne + 3][ligne + 3] == jeton)) {
                    if (this.matrice[colonne + 2][ligne + 1] != 0) {
                        resultat += 1333;
                    }
                }
                if ((this.matrice[colonne][ligne] == jeton && this.matrice[colonne + 1][ligne + 1] == 0
                        && this.matrice[colonne + 2][ligne + 2] == jeton
                        && this.matrice[colonne + 3][ligne + 3] == jeton)) {
                    if (this.matrice[colonne + 1][ligne] != 0) {
                        resultat += 1333;
                    }
                }
                if ((this.matrice[colonne][ligne] == 0 && this.matrice[colonne + 1][ligne + 1] == jeton
                        && this.matrice[colonne + 2][ligne + 2] == jeton
                        && this.matrice[colonne + 3][ligne + 3] == jeton)) {
                    if (ligne == 0)
                        resultat += 1333;
                    else {
                        if (this.matrice[colonne][ligne - 1] != 0) {
                            resultat += 1333;
                        }
                    }
                }
            }
        }
        for (int ligne = 0; ligne < NB_LIGNES - 3; ligne++) {
            for (int colonne = NB_COLONNES - 1; colonne >= 3; colonne--) {
                if (this.matrice[colonne][ligne] == jeton && this.matrice[colonne - 1][ligne + 1] == jeton
                        && this.matrice[colonne - 2][ligne + 2] == jeton && this.matrice[colonne - 3][ligne + 3] == 0) {
                    if (this.matrice[colonne - 3][ligne + 2] != 0)
                        resultat += 1333;
                }
                if (this.matrice[colonne][ligne] == jeton && this.matrice[colonne - 1][ligne + 1] == jeton
                        && this.matrice[colonne - 2][ligne + 2] == 0 && this.matrice[colonne - 3][ligne + 3] == jeton) {
                    if (this.matrice[colonne - 2][ligne + 1] != 0)
                        resultat += 1333;
                }
                if (this.matrice[colonne][ligne] == jeton && this.matrice[colonne - 1][ligne + 1] == 0
                        && this.matrice[colonne - 2][ligne + 2] == jeton
                        && this.matrice[colonne - 3][ligne + 3] == jeton) {
                    if (this.matrice[colonne - 1][ligne] != 0)
                        resultat += 1333;
                }
                if (this.matrice[colonne][ligne] == 0 && this.matrice[colonne - 1][ligne + 1] == jeton
                        && this.matrice[colonne - 2][ligne + 2] == jeton
                        && this.matrice[colonne - 3][ligne + 3] == jeton) {
                    if (ligne == 0)
                        resultat += 1333;
                    else {
                        if (this.matrice[colonne][ligne - 1] != 0)
                            resultat += 1333;
                    }
                }
            }
        }
        return resultat;
    }

    public void evaluer() {
        this.h = 0;
        this.h += -2 * this.quatrePionsAlignesLigne(false);
        this.h += this.quatrePionsAlignesLigne(true);
        this.h += -2 * this.quatrePionsAlignesColonne(false);
        this.h += this.quatrePionsAlignesColonne(true);
        this.h += -2 * this.quatrePionsPossiblesLigne(false);
        this.h += this.quatrePionsPossiblesLigne(true);
        this.h += -2 * this.quatrePionsPossiblesColonne(false);
        this.h += this.quatrePionsPossiblesColonne(true);

        this.h += -2 * this.quatrePionsAlignesDiagonal(false);
        this.h += this.quatrePionsAlignesDiagonal(true);
        this.h += -2 * this.quatrePionsPossiblesDiagonal(false);
        this.h += this.quatrePionsPossiblesDiagonal(true);
    }

}
