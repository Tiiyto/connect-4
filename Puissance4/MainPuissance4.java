import java.util.Scanner;

public class MainPuissance4 {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int m[][] = new int[7][6];
        Noeud racine = new Noeud(true, m);

        Puissance4 p3 = new Puissance4();

        int choix = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println(p3);
        do {
            System.out.println("Veuillez donner votre choix");
            String str = sc.nextLine();
            choix = Integer.parseInt(str);
            boolean j = p3.jouer(false, choix, p3.getMatriceJeu());
            while (!j) {
                System.out.println("Veuillez donner votre choix valide");
                str = sc.nextLine();
                choix = Integer.parseInt(str);
            }
            System.out.println(p3);

            if (p3.estFinJeu(false, p3.getMatriceJeu())) {
                System.out.println(" Vous avez gagné  !\n");
                break;
            }

            racine = new Noeud(true, p3.getMatriceJeu());

            Coup c = p3.alpha_beta(racine, Integer.MIN_VALUE, Integer.MAX_VALUE, 4);
            racine.setH(c.getEval());

            p3.jouer(true, c.getColonne(), p3.getMatriceJeu());
            System.out.println(p3);

            if (p3.estFinJeu(true, p3.getMatriceJeu())) {
                System.out.println("L'IA a gagné  !\n");
                break;
            }

        } while (true);
    }
}
