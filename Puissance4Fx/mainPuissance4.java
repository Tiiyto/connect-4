import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Button;

public class mainPuissance4 extends Application {

    @Override
    public void start(Stage stage) {
        Puissance4 p3 = new Puissance4();
        Button button1 = new Button("Rejouer");
        button1.setTranslateX(190);
        button1.setTranslateY(465);
        Text text = new Text(75, 40, "Jeu : Puissance 4");
        Text text1 = new Text(75, 90, "Cliquez sur la colonne ou vous souhaitez jouer");
        text.setFont(new Font(40));
        text1.setFont(new Font(15));
        Group group = new Group();
        Scene scene = new Scene(group, 450, 500, Color.LIGHTGREY);
        group.getChildren().add(text);
        group.getChildren().add(text1);
        group.getChildren().add(button1);
        Rectangle[][] tabr = new Rectangle[7][6];
        for (int i = 0; i < tabr.length; i++) {
            for (int j = 0; j < tabr[0].length; j++) {
                tabr[i][j] = new Rectangle();
                tabr[i][j].setX(50 * (i + 1));
                tabr[i][j].setY(450 - ((j + 1) * 50));
                tabr[i][j].setWidth(50);
                tabr[i][j].setHeight(50);
                tabr[i][j].setFill(Color.WHITE);
                tabr[i][j].setStroke(Color.BLACK);
                group.getChildren().add(tabr[i][j]);
            }
        }
        button1.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                if (p3.estFinJeu(false, p3.getMatriceJeu()) || p3.estFinJeu(true, p3.getMatriceJeu())) {
                    group.getChildren().remove(group.getChildren().size() - 1);
                }
                for (int k = 0; k < tabr.length; k++) {
                    for (int j = 0; j < tabr[0].length; j++) {
                        p3.getMatriceJeu()[k][j] = 0;
                    }
                }
                for (int k = 0; k < tabr.length; k++) {
                    for (int j = 0; j < tabr[0].length; j++) {
                        tabr[k][j].setFill(Color.WHITE);
                    }
                }
            }
        });
        Noeud racine = new Noeud(true, p3.getMatriceJeu());
        Coup c = p3.alpha_beta(racine, Integer.MIN_VALUE, Integer.MAX_VALUE, 4);
        racine.setH(c.getEval());
        p3.jouer(true, c.getColonne(), p3.getMatriceJeu());
        for (int k = 0; k < tabr.length; k++) {
            for (int j = 0; j < tabr[0].length; j++) {
                if (p3.getMatriceJeu()[k][j] == 1)
                    tabr[k][j].setFill(Color.RED);
                if (p3.getMatriceJeu()[k][j] == 2)
                    tabr[k][j].setFill(Color.BLUE);
                if (p3.getMatriceJeu()[k][j] == 0)
                    tabr[k][j].setFill(Color.WHITE);
            }
        }
        group.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if (!p3.estFinJeu(false, p3.getMatriceJeu()) && !p3.estFinJeu(true, p3.getMatriceJeu())) {
                    double y = event.getY();
                    if (y < 451 && y > 149) {
                        double x = event.getX();
                        for (int i = 0; i < 7; i++) {
                            if ((50 * (i + 1) - 1) < x && x < (50 + (50 * (i + 1) - 1))) {
                                boolean test = p3.jouer(false, i, p3.getMatriceJeu());
                                if (!test)
                                    break;
                                for (int k = 0; k < tabr.length; k++) {
                                    for (int j = 0; j < tabr[0].length; j++) {
                                        if (p3.getMatriceJeu()[k][j] == 1)
                                            tabr[k][j].setFill(Color.RED);
                                        if (p3.getMatriceJeu()[k][j] == 2)
                                            tabr[k][j].setFill(Color.BLUE);
                                        if (p3.getMatriceJeu()[k][j] == 0)
                                            tabr[k][j].setFill(Color.WHITE);
                                    }
                                }
                                if (p3.estFinJeu(false, p3.getMatriceJeu())) {
                                    Text gagn = new Text(75, 130, "Vous avez gagne");
                                    gagn.setFill(Color.RED);
                                    gagn.setFont(new Font(40));
                                    group.getChildren().add(gagn);
                                    break;
                                }
                                Noeud racine = new Noeud(true, p3.getMatriceJeu());
                                Coup c = p3.alpha_beta(racine, Integer.MIN_VALUE, Integer.MAX_VALUE, 4);
                                racine.setH(c.getEval());
                                p3.jouer(true, c.getColonne(), p3.getMatriceJeu());
                                for (int k = 0; k < tabr.length; k++) {
                                    for (int j = 0; j < tabr[0].length; j++) {
                                        if (p3.getMatriceJeu()[k][j] == 1)
                                            tabr[k][j].setFill(Color.RED);
                                        if (p3.getMatriceJeu()[k][j] == 2)
                                            tabr[k][j].setFill(Color.BLUE);
                                        if (p3.getMatriceJeu()[k][j] == 0)
                                            tabr[k][j].setFill(Color.WHITE);
                                    }
                                }
                                if (p3.estFinJeu(true, p3.getMatriceJeu())) {
                                    Text gagn = new Text(110, 130, "L'IA a gagne !");
                                    gagn.setFill(Color.RED);
                                    gagn.setFont(new Font(40));
                                    group.getChildren().add(gagn);
                                    break;

                                }
                            }
                        }
                    }
                }
            }
        });
        stage.setTitle("Puissance 4");
        stage.setScene(scene);
        stage.sizeToScene();
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}