
public class Puissance4 {

    private int[][] matriceJeu;
    public int WIDTH = 7;
    public int HEIGHT = 6;

    public Puissance4() {
        this.matriceJeu = new int[WIDTH][HEIGHT];
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                this.matriceJeu[i][j] = 0;
            }
        }
    }

    public int[][] getMatriceJeu() {
        return this.matriceJeu;
    }

    public boolean jouer(boolean typeJoueur, int colonne, int[][] matrice) {
        int jeton;
        if (typeJoueur)
            jeton = 1;
        else
            jeton = 2;
        int i = 0;
        boolean aJouer = false;
        while (i < matrice[i].length && !aJouer) {
            if (matrice[colonne][i] == 0) {
                matrice[colonne][i] = jeton;
                aJouer = true;
            }
            i++;
        }
        return aJouer;
    }

    public boolean estFinJeu(boolean typeJoueur, int[][] matrice) {
        boolean Fin = false;
        boolean MatricePleine = true;
        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice[i].length; j++) {
                if (matrice[i][j] == 0) {
                    MatricePleine = false;
                }
            }
        }
        Noeud n = new Noeud(typeJoueur, matrice);
        if (n.quatrePionsAlignesColonne(typeJoueur) == 6666 || n.quatrePionsAlignesLigne(typeJoueur) == 6666
                || n.quatrePionsAlignesDiagonal(typeJoueur) == 6666)
            Fin = true;
        if (MatricePleine)
            Fin = true;
        return Fin;
    }

    public String toString() {
        String r = "Grille de jeu :\n \n";
        int[][] matriceAffichage = new int[HEIGHT][WIDTH];
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                matriceAffichage[j][i] = this.matriceJeu[i][j];
            }
        }
        for (int i = HEIGHT - 1; i >= 0; i--) {
            for (int j = 0; j < WIDTH; j++) {
                r += matriceAffichage[i][j] + "   ";
            }
            r += "\n \n";
        }
        return r;
    }

    public void copieMatrice(int[][] mSource, int[][] mDest) {
        for (int i = 0; i < mSource.length; i++) {
            for (int j = 0; j < mSource[i].length; j++) {
                mDest[i][j] = mSource[i][j];
            }
        }
    }

    public Coup alpha_beta(Noeud racine, int alpha, int beta, int profondeur) {
        int bestj = 0;
        if (profondeur == 1 || estFinJeu(!racine.isMax(), racine.getMatrice())) {
            racine.evaluer();
            return new Coup(racine.getH(), -1);
        }
        if (racine.isMax()) {
            for (int i = 0; i < this.WIDTH; i++) {
                int[][] matriceC = new int[WIDTH][HEIGHT];
                copieMatrice(racine.getMatrice(), matriceC);
                if (jouer(racine.isMax(), i, matriceC)) {
                    Noeud successeur = new Noeud(!racine.isMax(), matriceC);
                    Coup coup = alpha_beta(successeur, alpha, beta, profondeur - 1);
                    successeur.setH(coup.getEval());
                    if (coup.getEval() > alpha) {
                        alpha = coup.getEval();
                        bestj = i;
                    }
                    if (alpha >= beta)
                        return new Coup(alpha, i);
                }
            }
            return new Coup(alpha, bestj);
        } else {
            for (int i = 0; i < this.WIDTH; i++) {
                int[][] matriceC = new int[WIDTH][HEIGHT];
                copieMatrice(racine.getMatrice(), matriceC);
                if (jouer(racine.isMax(), i, matriceC)) {
                    Noeud successeur = new Noeud(!racine.isMax(), matriceC);
                    Coup coup = alpha_beta(successeur, alpha, beta, profondeur - 1);
                    if (coup.getEval() < beta) {
                        beta = coup.getEval();
                        bestj = i;
                    }
                    if (alpha >= beta)
                        return new Coup(beta, i);
                }
            }
            return new Coup(beta, bestj);
        }
    }
}
